import React from "react";
import LandingPage from "../component/LandingPage";
import OurService from "../component/OurService";
import WhyUs from '../component/WhyUs';
import Testimony from "../component/Testimony";
import Banner from "../component/Banner";
import Faq from "../component/Faq";
import Footer from "../component/Footer"
import { useEffect } from "react";

function Home() {
    useEffect(() => {
        document.title = "Homepage";
    }, []);

    return (
        <div>
            <LandingPage />
            <OurService />
            <WhyUs />
            <Testimony />
            <Banner />
            <Faq />
            <Footer />
        </div>
    );
}

export default Home;
