import React from "react";
import LandingPageNoButton from "../component/LandingPageNoButton";
import SearchCar from "../component/SearchCar";

import Footer from "../component/Footer"

function Car() {
    return (
        <div>
            <LandingPageNoButton />
            <SearchCar />

            <Footer />
        </div>
    );
}

export default Car;
