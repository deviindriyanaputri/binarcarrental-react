import React from 'react';
import {
  Container,
  Navbar,
  Nav,
  Button,
  Offcanvas
} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Navbar.css'

class NavNavbar extends React.Component {
  render() {
    return (
      <Navbar expand="lg" className="mb-3 blue-1">
        <Container fluid>
          <Navbar.Brand href="#" ><img className="logo ms-5" src="/gambar/logo.png" alt="" /></Navbar.Brand>
          <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-lg`} />
          <Navbar.Offcanvas
            id={`offcanvasNavbar-expand-lg`}
            aria-labelledby={`offcanvasNavbarLabel-expand-lg`}
            placement="end"
          >
            <Offcanvas.Header closeButton>
              <Offcanvas.Title id={`offcanvasNavbarLabel-expand-lg`}>
                BCR-REACT
              </Offcanvas.Title>
            </Offcanvas.Header>
            <Offcanvas.Body>
              <Nav className="justify-content-end flex-grow-1 pe-3">
                <Nav.Link href="#our-service" className="nav-link mx-2 text-black">Our Service</Nav.Link>
                <Nav.Link href="#why-us" className="nav-link mx-2 text-black">Why Us</Nav.Link>
                <Nav.Link href="#testimonial" className="nav-link mx-2 text-black">Testimony</Nav.Link>
                <Nav.Link href="#faq" className="nav-link mx-2 text-black">FAQ</Nav.Link>
                <Button className='custom-button fw-bold text-white' variant="success">Register</Button>
              </Nav>
            </Offcanvas.Body>
          </Navbar.Offcanvas>
        </Container>
      </Navbar>

    )
  };
};

export default NavNavbar;
