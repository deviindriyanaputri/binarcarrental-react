import React from 'react';
import {
    Container,
    Row,
    Col,
    Button,
} from 'react-bootstrap';
import { Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import './HeroSection.css';


class Hero extends React.Component {
    render() {
        return (
            <Container className='hero'>
                <Row className='row-hero'>
                    <Col className='col-md'>
                        <br></br>
                        <br></br>
                        <h1>Sewa & Rental Mobil Terbaik di kawasan(Pati Pride)</h1>
                        <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga
                            terjangkau. Selalu
                            siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                        <Link to="/cars">
                            <Button className='custom-button fw-bold' variant="success">Mulai Sewa Mobil</Button>
                        </Link>
                    </Col>
                    <Col>
                        <img src={process.env.PUBLIC_URL + '/gambar/img_car.png'} />
                    </Col>
                </Row>
            </Container>
        );
    };
};

export default Hero;
