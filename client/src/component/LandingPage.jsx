import React from 'react'
import NavNavbar from "./Navbar";
import Hero from "./HeroSection";
import './LandingPage.css';

class LandingPage extends React.Component {
    render() {
        return (
            <div className="hero-section">
                <NavNavbar />
                <Hero />
            </div>
        );
    }
}

export default LandingPage;

