import React from 'react';
import {
    Container,
    Row,
    Col
} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './OurService.css';

class OurService extends React.Component {
    render() {
        return (
            <Container className='service' id='our-service'>
                <Row className='row-ourService'>
                    <Col className='col-md desc'>
                        <img src={process.env.PUBLIC_URL + '/gambar/img_service.png'} className='img-fluid' />
                    </Col>
                    <Col className='col-md desc'>
                        <h2>Best Car Rental for any kind of trip in (Lokasimu)!</h2>
                        <p>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain,
                            kondisi
                            mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.
                        </p>
                        <ul>
                            <li><img src={process.env.PUBLIC_URL + '/gambar/Group 53.png'} />&ensp; Sewa Mobil Dengan Supir di Bali 12 Jam</li>
                            <li><img src={process.env.PUBLIC_URL + '/gambar/Group 53.png'} />&ensp; Sewa Mobil Lepas Kunci di Bali 24 Jam</li>
                            <li><img src={process.env.PUBLIC_URL + '/gambar/Group 53.png'} />&ensp; Sewa Mobil Jangka Panjang Bulanan</li>
                            <li><img src={process.env.PUBLIC_URL + '/gambar/Group 53.png'} />&ensp; Gratis Antar - Jemput Mobil di Bandara</li>
                            <li><img src={process.env.PUBLIC_URL + '/gambar/Group 53.png'} />&ensp; Layanan Airport Transfer / Drop In Out</li>
                        </ul>
                    </Col>
                </Row>

            </Container>
        );
    };
};

export default OurService;
