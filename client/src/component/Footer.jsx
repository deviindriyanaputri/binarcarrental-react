import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Footer.css'

class Footer extends React.Component {
    render() {
        return (
            <div className="container footer">
                <div className="row">
                    <div className="col-md">
                        <div className="col-md col-text">
                            <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                            <p>binarcarrental@gmail.com</p>
                            <p>081-233-334-808</p>
                        </div>
                    </div>
                    <div className="col-md ">
                        <div className="col-md col-text">
                            <p><a href="#our-service" className="link-dark">Our Services</a></p>
                            <p><a href="#why-us" className="link-dark">Why Us</a></p>
                            <p><a href="#testimonial" className="link-dark">Testimonial</a></p>
                            <p><a href="#faq" className="link-dark">FAQ</a></p>
                        </div>
                    </div>
                    <div className="col-md">
                        <div className="col-md col-text">
                            <p>Connect with us</p>
                            <div className="icon">
                                <a href="#"><img src={process.env.PUBLIC_URL + '/gambar/icon_facebook.png'} /></a>
                                <a href="#"><img src={process.env.PUBLIC_URL + '/gambar/icon_instagram.png'} /></a>
                                <a href="#"><img src={process.env.PUBLIC_URL + '/gambar/icon_twitter.png'} /></a>
                                <a href="#"><img src={process.env.PUBLIC_URL + '/gambar/icon_mail.png'} /></a>
                                <a href="#"><img src={process.env.PUBLIC_URL + '/gambar/icon_twitch.png'} /></a>
                            </div>
                        </div>
                    </div>
                    <div className="col-md">
                        <div className="col-md col-text">
                            <p>Copyright Binar 2022</p>
                            <img src={process.env.PUBLIC_URL + '/gambar/logo.png'} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Footer;
