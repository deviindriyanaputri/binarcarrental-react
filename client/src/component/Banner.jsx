import React from 'react';
import {
    Container, Button
} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Banner.css'

class Banner extends React.Component {
    render() {
        return (
            <Container className='banner'>
                <div id="ban-banner" className="ban-banner d-flex flex-column align-items-center">
                    <h2 className="banner-text text-center">Sewa Mobil di (Pati Pride) Sekarang Juga</h2>
                    <p className="mt-4 text-center">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod <br />
                        tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                    <Button className='custom-button' variant="success">Mulai Sewa Mobil</Button>
                </div>
            </Container>
        )
    }
}

export default Banner;
