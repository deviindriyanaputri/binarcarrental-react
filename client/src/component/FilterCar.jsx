import React from 'react';
import {
    Button
} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './FilterCar.css'

const FilterCar = (props) => {
    // console.log(props.cars);
    return (
        <>
            <div className="container">
                <div className="cars-container row mt-5">
                    {props.cars && props.cars.map((car) => (
                        <div className="col-md-4 my-2" key={car.id}>
                            <div className="card h-100">
                                <div className="card-body">
                                    <img src={car.image} className="card-img" />
                                    <h6 className="card-title mt-3 card-title-text">
                                        {car.manufacture}/{car.model}
                                    </h6>
                                    <h5 className="card-title mt-3 card-title-text">Rp {car.rentPerDay} / hari</h5>
                                    <p className="card-text">{car.description}
                                    </p>
                                    <h6>
                                        <i className="bi bi-people" style={{ fontSize: 20 + "px" }}></i> {car.capacity} orang
                                    </h6>
                                    <h6>
                                        <i className="bi bi-gear" style={{ fontSize: 20 + "px" }}></i> {car.transmission}
                                    </h6>
                                    <h6>
                                        <i className="bi bi-calendar" style={{ fontSize: 20 + "px" }}></i> Tahun {car.year}
                                    </h6>
                                    <div className="d-flex flex-column mt-3 align-items-stretch">
                                        <Button className='custom-button' type="submit" variant="success">Pilih Mobil</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))
                    }
                </div>
            </div>
        </>
    );
};


export default FilterCar;
