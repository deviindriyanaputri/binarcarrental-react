import React from 'react';
import {
    Container,
    Row,
    Col,
    Card
} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './WhyUs.css';

class WhyUs extends React.Component {
    render() {
        return (
            <Container className='why' id='why-us'>
                <Row>
                    <Col className='text-card'>
                        <h2>Why Us?</h2>
                        <br></br>
                        <p>Mengapa harus pilih Binar Car Rental?</p>
                    </Col>
                </Row>
                <Row mt={4} className='row-why row justify-content-center'>
                    <Col className='col-md'>
                        <Card className='card-why'>
                            <Card.Img variant="top" />
                            <Card.Body>
                                <img src={process.env.PUBLIC_URL + '/gambar/icon_complete.png'} className="card-img-why" alt='complete' />
                                <Card.Title>Mobil Lengkap</Card.Title>
                                <Card.Text>
                                    Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col className='col-md'>
                        <Card className='card-why'>
                            <Card.Img variant="top" />
                            <Card.Body>
                                <img src={process.env.PUBLIC_URL + '/gambar/icon_price.png'} className="card-img-why" alt='complete' />
                                <Card.Title>Harga Murah</Card.Title>
                                <Card.Text>
                                    Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil
                                    lain
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col className='col-md'>
                        <Card className='card-why'>
                            <Card.Img variant="top" />
                            <Card.Body>
                                <img src={process.env.PUBLIC_URL + '/gambar/icon_24hrs.png'} className="card-img-why" alt='complete' />
                                <Card.Title>Layanan 24 Jam</Card.Title>
                                <Card.Text>
                                    Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di
                                    akhir minggu
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col className='col-md'>
                        <Card className='card-why'>
                            <Card.Img variant="top" />
                            <Card.Body>
                                <img src={process.env.PUBLIC_URL + '/gambar/icon_professional.png'} className="card-img-why" alt='complete' />
                                <Card.Title>Sopir Profesional</Card.Title>
                                <Card.Text>
                                    Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        );
    };
};

export default WhyUs;
