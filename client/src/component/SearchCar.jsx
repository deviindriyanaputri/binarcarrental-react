import React from 'react';
import { useEffect, useState } from "react";
import FilterCar from './FilterCar';
import { useSelector, useDispatch } from "react-redux";
import { fetchUsers } from "../redux";
import {
    Button
} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './SearchCar.css'

const SearchCar = () => {
    const [type, setType] = useState("Pilih Tipe Driver");
    const [date, setDate] = useState("Pilih Waktu");
    const [pickupTime, setPickupTime] = useState("8");
    const [passenger, setPassenger] = useState("");
    const dispatch = useDispatch();
    const state = useSelector((state) => state);
    console.log(state);

    const handleSubmit = (e) => {
        console.log("searchButton")
        e.preventDefault();
        if (type !== "Pilih Tipe Driver") {
            const pass = passenger ? passenger : "0";
            const filter = { type, date, pickupTime, pass };
            dispatch(fetchUsers(filter));
        }
    };

    const handleType = (e) => {
        setType(e.target.value);
    };
    const handleDate = (e) => {
        setDate(e.target.value);
    };
    const handlePickupTime = (e) => {
        setPickupTime(e.target.value);
    };
    const handlePassenger = (e) => {
        setPassenger(e.target.value);
    };

    useEffect(() => {
        console.log(state.cars);
    }, [state]);

    return (
        <>
            <div class="cari-mobil container border bg-white rounded p-3">
                <form onSubmit={handleSubmit}>
                    <div class="row gx-3 gy-2 align-items-center">
                        <div class="mobil col-sm-2">
                            <label for="typeDriver" class="form-label">Tipe Driver</label>
                            <select value={type} required onChange={handleType} id="typeDriver" name="typeDriver" className="form-select type">
                                <option disabled hidden>Pilih Tipe Driver</option>
                                <option value="Dengan Sopir">Dengan Sopir</option>
                                <option value="Keyless Entry">Tanpa Sopir (Lepas Kunci)</option>
                            </select>
                        </div>
                        <div class="mobil col-sm-2">
                            <label for="inputTipeDriver" class="form-label">Tanggal</label>
                            <input id="date" type="date" name="date" class="form-select" onChange={handleDate} required />
                        </div>
                        <div className="mobil col-sm-3">
                            <label for="time" class="form-label clock">Waktu Ambil</label>
                            <select value={pickupTime} onChange={handlePickupTime} className="form-select time" name="time" id="time">
                                <option value="8">08.00 WIB</option>
                                <option value="9">09.00 WIB</option>
                                <option value="10">10.00 WIB</option>
                                <option value="11">11.00 WIB</option>
                                <option value="12">12.00 WIB</option>
                            </select>
                        </div>
                        <div class="mobil col-sm-3">
                            <label for="passenger" class="form-label">Jumlah Penumpang (Optional)</label>
                            <label class="visually-hidden" for="specificSizeInputGroupUsername">Jumlah Penumpang (Optional)</label>
                            <div class="input-group">
                                <input value={passenger} onChange={handlePassenger} id="passenger" type="text" name="passenger" class="form-control"
                                    placeholder="Jumlah Penumpang" />
                                <div class="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-people-fill" viewBox="0 0 16 16">
                                        <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                                        <path fill-rule="evenodd"
                                            d="M5.216 14A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216z" />
                                        <path d="M4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-2 mt-5 d-flex flex-column justify-content-center">
                            <Button className='custom-button' type="submit" variant="success">Cari</Button>
                        </div>
                    </div>
                </form>
            </div>
            <br></br>
            <br></br>
            <br></br>
            {state.cars && <FilterCar cars={state.cars} />}
            {/* {console.log(state.cars)} */}
        </>
    );
};


export default SearchCar;
