import React from 'react'
import NavNavbar from "./Navbar";
import HeroNoButton from './HeroNoButton';
import './LandingPage.css';

class LandingPageNoButton extends React.Component {
    render() {
        return (
            <div className="hero-section">
                <NavNavbar />
                <HeroNoButton />
            </div>
        );
    }
}

export default LandingPageNoButton;