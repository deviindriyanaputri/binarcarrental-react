import React from 'react';
import {
    Container,
    Row,
    Col
} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Faq.css'

class Faq extends React.Component {
    render() {
        return (
            <Container className='faq' id='faq'>
                <Row className='row-faq'>
                    <Col className='text-faq col-md'>
                        <h1>Frequently Asked Question</h1>
                        <p className='p-text'>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                    </Col>

                    {/* Accordion */}

                    <div class="col-md">
                        <div class="accordion" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Apa saja syarat yang dibutuhkan?
                                    </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
                                    data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <strong>This is the first item's accordion body.</strong> It is shown by default, until
                                        the collapse
                                        plugin adds the appropriate classes that we use to style each element. These classes
                                        control the
                                        overall appearance, as well as the showing and hiding via CSS transitions. You can
                                        modify any of
                                        this
                                        with custom CSS or overriding our default variables. It's also worth noting that just
                                        about any HTML
                                        can go within the <code>.accordion-body</code>, though the transition does limit
                                        overflow.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Berapa hari minimal sewa mobil lepas kunci?
                                    </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                                    data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <strong>This is the second item's accordion body.</strong> It is hidden by default,
                                        until the
                                        collapse
                                        plugin adds the appropriate classes that we use to style each element. These classes
                                        control the
                                        overall appearance, as well as the showing and hiding via CSS transitions. You can
                                        modify any of
                                        this
                                        with custom CSS or overriding our default variables. It's also worth noting that just
                                        about any HTML
                                        can go within the <code>.accordion-body</code>, though the transition does limit
                                        overflow.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingThree">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Berapa hari sebelumnya sabaiknya booking sewa mobil?
                                    </button>
                                </h2>
                                <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree"
                                    data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <strong>This is the third item's accordion body.</strong> It is hidden by default, until
                                        the
                                        collapse
                                        plugin adds the appropriate classes that we use to style each element. These classes
                                        control the
                                        overall appearance, as well as the showing and hiding via CSS transitions. You can
                                        modify any of
                                        this
                                        with custom CSS or overriding our default variables. It's also worth noting that just
                                        about any HTML
                                        can go within the <code>.accordion-body</code>, though the transition does limit
                                        overflow.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Apakah Ada biaya antar-jemput?
                                    </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                                    data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <strong>This is the second item's accordion body.</strong> It is hidden by default,
                                        until the
                                        collapse
                                        plugin adds the appropriate classes that we use to style each element. These classes
                                        control the
                                        overall appearance, as well as the showing and hiding via CSS transitions. You can
                                        modify any of
                                        this
                                        with custom CSS or overriding our default variables. It's also worth noting that just
                                        about any HTML
                                        can go within the <code>.accordion-body</code>, though the transition does limit
                                        overflow.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Bagaimana jika terjadi kecelakaan
                                    </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                                    data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <strong>This is the second item's accordion body.</strong> It is hidden by default,
                                        until the
                                        collapse
                                        plugin adds the appropriate classes that we use to style each element. These classes
                                        control the
                                        overall appearance, as well as the showing and hiding via CSS transitions. You can
                                        modify any of
                                        this
                                        with custom CSS or overriding our default variables. It's also worth noting that just
                                        about any HTML
                                        can go within the <code>.accordion-body</code>, though the transition does limit
                                        overflow.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Row>
            </Container>
        )
    };
};

export default Faq;
